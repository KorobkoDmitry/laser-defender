﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoseScores : MonoBehaviour
{
    public int score = 0;

    private Text scoreValue;

    private void Start()
    {
        scoreValue = GetComponent<Text>();
        scoreValue.text = ScoreKeeper.GetScores().ToString();
    }    
}
