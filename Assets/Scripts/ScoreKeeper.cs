﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreKeeper : MonoBehaviour {

    public static int score = 0;

    private Text scoreValue;

    private void Start()
    {
        scoreValue = GetComponent<Text>();
        Reset();
    }

    public void Score(int points)
    {
        score += points;
        scoreValue.text = score.ToString();
    }

    public void Reset()
    {
        score = 0;
        scoreValue.text = score.ToString();
    }

    public static int GetScores()
    {
        return score;
    }
}
